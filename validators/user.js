const Joi = require('joi');

const userValidator = {
  addOrEditEmployee: Joi.object({
    username: Joi.string().required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().email().required(),
    birthDate: Joi.date().required(),
    basicSalary: Joi.number().required(),
    status: Joi.number().valid(1, 2).required(),
    group: Joi.string().required(),
    description: Joi.string().required(),
  }),
};

module.exports = userValidator;
