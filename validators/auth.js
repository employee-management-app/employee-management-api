const Joi = require('joi');

const authValidator = {
  registerAdminUser: Joi.object({
    username: Joi.string().trim().required(),
    password: Joi.string().trim().required(),
  }),
  loginAdminUser: Joi.object({
    username: Joi.string().trim().required(),
    password: Joi.string().trim().required(),
  }),
};

module.exports = authValidator;
