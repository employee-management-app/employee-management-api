const app = require('express').Router();
const { Unauthorized } = require('../utils/errorHandler');

app.get('/fail', (_, res) => {
  Unauthorized(res);
});

module.exports = app;
