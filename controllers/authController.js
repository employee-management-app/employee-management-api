const app = require('express').Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { registerAdminUser, loginAdminUser } = require('../validators/auth');
const { UnHandler, BadRequest } = require('../utils/errorHandler');
const { userDetail } = require('../transformers/user');
const { payloadJwt } = require('../transformers/auth');
const comparePasswordAsync = require('../utils/comparePasswordAsync');
const passportHandler = require('../utils/passportHandler');

const User = require('../models/user');

const privateKey = process.env.PRIVATE_KEY;

app.post('/register-admin-user', async (req, res) => {
  try {
    const validate = registerAdminUser.validate(req.body);
    if (validate.error) throw validate.error;

    const findUser = await User.findOne({ username: req.body.username, role: 'admin', status: 1 }).lean();
    if (findUser) return BadRequest(res, 'User already exist');

    bcrypt.genSalt(10, (error1, salt) => {
      if (error1) throw error1;

      bcrypt.hash(req.body.password, salt, async (error2, hash) => {
        if (error2) throw error2;

        const user = new User({
          role: 'admin',
          username: req.body.username,
          password: hash,
        });

        const newUser = await user.save();
        const result = userDetail(newUser);
        const jwtPayload = payloadJwt(result);

        jwt.sign(jwtPayload, privateKey, { expiresIn: 3600 * 24 * 30 }, (error3, token) => {
          if (error3) throw error3;

          res.status(201).json({ status: 'OK', accessToken: token, result });
        });
      });
    });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.post('/login-admin-user', async (req, res) => {
  try {
    const validate = loginAdminUser.validate(req.body);
    if (validate.error) throw validate.error;

    const user = await User.findOne({ username: req.body.username, status: 1 }).lean();
    if (!user) return BadRequest(res, 'Invalid username or password');
    if (!user.role === 'admin') return BadRequest("You don't have role as admin");

    const userPassword = user.password;

    const passwordIsMatch = await comparePasswordAsync(req.body.password, userPassword);
    if (!passwordIsMatch) return BadRequest(res, 'Password is incorrect');

    const result = userDetail(user);
    const jwtPayload = payloadJwt(result);

    jwt.sign(jwtPayload, privateKey, { expiresIn: 3600 * 24 * 30 }, (error, token) => {
      if (error) throw error;

      res.json({ status: 'OK', accessToken: token, result });
    });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.post('/logout', passportHandler, async (req, res) => {
  try {
    req.logout((error) => {
      if (error) throw error;
      res.json({ status: 'OK', message: 'See you' });
    });
  } catch (error) {
    UnHandler(res, error);
  }
});

module.exports = app;
