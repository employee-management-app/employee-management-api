const app = require('express').Router();

const { UnHandler, BadRequest, NotFound } = require('../utils/errorHandler');
const { userAdmin } = require('../utils/userRoleValidator');
const { addOrEditEmployee } = require('../validators/user');
const { removeSpaceFromString, isValidDate, shuffleArray } = require('../utils/helper');
const { userDetail, userList } = require('../transformers/user');
const { startOfDay, endOfDay } = require('../utils/dateFormatter');
const dataMeta = require('../utils/dataMeta');
const passportHandler = require('../utils/passportHandler');
const replaceSqlInjection = require('../utils/replaceSqlInjection');

const User = require('../models/user');

app.post('/add', passportHandler, async (req, res) => {
  try {
    await userAdmin(req, res);

    const validate = addOrEditEmployee.validate(req.body);
    if (validate.error) throw validate.error;

    const existEmployee = await checkExistingEmployee(null, req.body.username, req.body.email);
    if (existEmployee) return BadRequest(res, existEmployee);

    const newEmployee = new User({
      role: 'employee',
      ...req.body,
    });

    const employee = await newEmployee.save();
    const result = userDetail(employee);

    res.status(201).json({ status: 'OK', result });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.get('/list', passportHandler, async (req, res) => {
  try {
    await userAdmin(req, res);

    const meta = {
      ...dataMeta(req),
      username: req.query.username === undefined ? null : req.query.username,
      firstName: req.query.firstName === undefined ? null : req.query.firstName,
      lastName: req.query.lastName === undefined ? null : req.query.lastName,
      email: req.query.email === undefined ? null : req.query.email,
      birthDate: req.query.birthDate === undefined ? null : req.query.birthDate,
      basicSalary: req.query.basicSalary === undefined ? null : req.query.basicSalary,
      status: req.query.status === undefined ? null : req.query.status,
      group: req.query.group === undefined ? null : req.query.group,
      sortBy: req.query.sortBy === undefined ? null : req.query.sortBy,
    };

    const query = {
      role: 'employee',
    };

    if (meta.username) {
      const username = new RegExp(replaceSqlInjection(meta.username), 'i');
      query.username = { $regex: username };
    }

    if (meta.firstName) {
      const firstName = new RegExp(replaceSqlInjection(meta.firstName), 'i');
      query.firstName = { $regex: firstName };
    }

    if (meta.lastName) {
      const lastName = new RegExp(replaceSqlInjection(meta.lastName), 'i');
      query.lastName = { $regex: lastName };
    }

    if (meta.email) {
      query.email = meta.email;
    }

    if (meta.birthDate) {
      const birthDate = new Date(meta.birthDate);
      if (!isValidDate(birthDate)) return BadRequest(res, 'birthDate filter must be valid date');

      const startDay = startOfDay(birthDate);
      const endDay = endOfDay(birthDate);

      query.birthDate = {
        $gte: startDay,
        $lte: endDay,
      };
    }

    if (meta.basicSalary) {
      const basicSalary = parseInt(meta.basicSalary);
      query.basicSalary = basicSalary;
    }

    if (meta.status) {
      const status = parseInt(meta.status);
      query.status = status;
    }

    if (meta.group) {
      const group = new RegExp(meta.group, 'i');
      query.group = { $regex: group };
    }

    let sortBy = { updated_at: -1 };
    if (meta.sortBy) {
      if (meta.sortBy === 'salary-asc') {
        sortBy = { basicSalary: -1 };
      } else if (meta.sortBy === 'salary-desc') {
        sortBy = { basicSalary: 1 };
      }
    }

    const options = {
      lean: true,
      sort: sortBy,
      page: meta.page,
      limit: meta.limit,
    };

    const result = await User.paginate(query, options);
    result.docs = userList(result.docs);
    result.meta = meta;

    res.json({ status: 'OK', result });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.get('/detail/:employee_id', passportHandler, async (req, res) => {
  try {
    await userAdmin(req, res);

    const user = await User.findOne({ _id: req.params.employee_id, role: 'employee' }).lean();
    if (!user) return NotFound(res, 'Employee not found');

    const result = userDetail(user);
    res.json({ status: 'OK', result });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.put('/edit/:employee_id', passportHandler, async (req, res) => {
  try {
    await userAdmin(req, res);

    const validate = addOrEditEmployee.validate(req.body);
    if (validate.error) throw validate.error;

    const user = await User.findOne({ _id: req.params.employee_id, role: 'employee' }).select(['_id', 'username', 'email', 'role']).lean();
    if (!user) return NotFound(res, 'Employee not found');

    const existEmployee = await checkExistingEmployee(req.params.employee_id, req.body.username, req.body.email);
    if (existEmployee) return BadRequest(res, existEmployee);

    const updatedUser = await User.findByIdAndUpdate(req.params.employee_id, {
      role: 'employee',
      ...req.body,
    }, { new: true }).lean();
    const result = userDetail(updatedUser);

    res.json({ status: 'OK', result });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.delete('/delete/:employee_id', passportHandler, async (req, res) => {
  try {
    await userAdmin(req, res);

    const user = await User.findOne({ _id: req.params.employee_id, role: 'employee' }).select('_id').lean();
    if (!user) return NotFound(res, 'Employee not found');

    const deletedUser = await User.findByIdAndDelete(req.params.employee_id, { new: true }).lean();
    const result = userDetail(deletedUser);

    res.json({ status: 'OK', result });
  } catch (error) {
    UnHandler(res, error);
  }
});

app.post('/inject-100-dummy-employee-data', passportHandler, async (req, res) => {
  try {
    await userAdmin(req, res);

    await User.deleteMany({ role: 'employee' }).lean();

    const employeeData = [];

    for (let i = 1; i <= 100; i++) {
      const birthDateData = ['2024-06-01', '2024-06-02', '2024-06-03', '2024-06-04', '2024-06-05'];
      const basicSalaryData = [1000000, 2000000, 3000000, 4000000, 5000000];
      const groupData = ['Marketing', 'Bussines Development', 'Software Developer', 'Community', 'Humman Resource'];

      employeeData.push({
        role: 'employee',
        username: `test${i}`,
        status: 1,
        firstName: 'Test',
        lastName: `${i}`,
        email: `test${i}@gmail.com`,
        birthDate: shuffleArray(birthDateData)[0],
        basicSalary: shuffleArray(basicSalaryData)[0],
        group: shuffleArray(groupData)[0],
        description: 'This is data test',
      });
    }

    if (employeeData.length > 0) {
      const result = await User.insertMany(employeeData);
      return res.status(201).json({ status: 'OK', result });
    }

    return res.json({ status: 'OK', result: 'No injected employee data' });
  } catch (error) {
    UnHandler(res, error);
  }
});

// Private function
const checkExistingEmployee = async (employeeId, username, email) => {
  if (typeof username !== 'string') return 'Username must be string';
  if (typeof email !== 'string') return 'Email must be string';

  let users = [];

  if (employeeId) {
    if (typeof employeeId !== 'string') return 'Employee id must be string';
    users = await User.find({ _id: { $ne: employeeId }, role: 'employee' }).select(['_id', 'username', 'email']).lean();
  } else {
    users = await User.find({ status: 1, role: 'employee' }).select(['_id', 'username', 'email']).lean();
  }

  if (Array.isArray(users) && users.length > 0) {
    if (findUserByUsername(users, username)) {
      return findUserByUsername(users, username);
    }

    if (findUserByEmail(users, email)) {
      return findUserByEmail(users, email);
    }
  }
};

const findUserByUsername = (users, username) => {
  if (!Array.isArray(users)) return 'Users must be array';
  if (typeof username !== 'string') return 'Username must be string';

  const findUser = users.find((data) => (data && data.username) && (removeSpaceFromString(data.username).toLocaleLowerCase() === removeSpaceFromString(username).toLocaleLowerCase()));
  if (findUser) return 'Username exist';
};

const findUserByEmail = (users, email) => {
  if (!Array.isArray(users)) return 'Users must be array';
  if (typeof email !== 'string') return 'Email must be string';

  const findUser = users.find((data) => (data && data.email) && (removeSpaceFromString(data.email).toLocaleLowerCase() === removeSpaceFromString(email).toLocaleLowerCase()));
  if (findUser) return 'Email exist';
};

module.exports = app;
