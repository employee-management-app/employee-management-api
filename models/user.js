const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate-v2');

const userSchema = require('../db/schemas/userSchema');

userSchema.plugin(mongoosePaginate);
userSchema.plugin(mongooseAggregatePaginate);

const user = mongoose.model('user', userSchema);

module.exports = user;
