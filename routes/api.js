const express = require('express');

const router = express.Router();

const authController = require('../controllers/authController');
const responseController = require('../controllers/responseController');
const employeeController = require('../controllers/employeeController');

router.use('/auth', authController);
router.use('/response', responseController);
router.use('/employee', employeeController);

module.exports = router;
