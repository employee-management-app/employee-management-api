const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  role: { type: String }, // admin, employee
  username: { type: String, unique: true },
  password: { type: String },
  status: { type: Number, default: 1 }, // 1 => active, 2 => inactive
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String, unique: true },
  birthDate: { type: Date },
  basicSalary: { type: Number },
  group: { type: String },
  description: { type: String },
}, {
  versionKey: false,
  toJSON: {
    virtuals: true,
  },
  toObject: {
    virtuals: true,
  },
  timestamps: true,
});

module.exports = userSchema;
