const mongoose = require('mongoose');

const NODE_ENV = process.env.NODE_ENV || 'development';
const MONGO_HOST = process.env.MONGO_HOST || 'localhost';
const MONGO_PORT = process.env.MONGO_PORT || '27107';
const MONGO_DATABASE = process.env.MONGO_DATABASE || 'employee-management';

const { MONGO_ATLAS_USER } = process.env;
const { MONGO_ATLAS_PASSWORD } = process.env;
const { MONGO_ATLAS_HOST_1 } = process.env;
const { MONGO_ATLAS_HOST_2 } = process.env;
const { MONGO_ATLAS_HOST_3 } = process.env;
const { MONGO_ATLAS_PORT } = process.env;
const { MONGO_ATLAS_DATABASE } = process.env;

let connectionUrl = null;

if (NODE_ENV === 'staging') {
  connectionUrl = `mongodb://${MONGO_ATLAS_USER}:${MONGO_ATLAS_PASSWORD}@${MONGO_ATLAS_HOST_1}:${MONGO_ATLAS_PORT}/${MONGO_ATLAS_DATABASE}?ssl=true&authSource=admin`;
} else if (NODE_ENV === 'replicaset') {
  connectionUrl = `mongodb://${MONGO_ATLAS_USER}:${MONGO_ATLAS_PASSWORD}@${MONGO_ATLAS_HOST_1}:${MONGO_ATLAS_PORT},${MONGO_ATLAS_HOST_2}:${MONGO_ATLAS_PORT},${MONGO_ATLAS_HOST_3}:${MONGO_ATLAS_PORT}/${MONGO_ATLAS_DATABASE}?replicaSet=atlas-vjjx2s-shard-0&ssl=true&authSource=admin`;
} else {
  connectionUrl = `mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}`;
}

mongoose.connect(connectionUrl);

const db = mongoose.connection;
// eslint-disable-next-line no-console
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  // eslint-disable-next-line no-console
  console.log('DB is ready', new Date());
});

module.exports = db;
