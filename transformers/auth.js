const authTransformer = {};
authTransformer.payloadJwt = (data) => payloadJwt(data);

const payloadJwt = (data) => ({
  _id: data && data._id || null,
});

module.exports = authTransformer;
