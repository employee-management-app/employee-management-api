const userTransformer = {};
userTransformer.userDetail = (data) => userDetail(data);
userTransformer.userList = (datas) => datas.map((data) => userDetail(data));

const userDetail = (data) => ({
  _id: data && data._id || null,
  role: data && data.role || null,
  username: data && data.username || null,
  firstName: data && data.firstName || null,
  lastName: data && data.lastName || null,
  email: data && data.email || null,
  birthDate: data && data.birthDate || null,
  basicSalary: data && data.basicSalary || null,
  status: data && data.status && userStatus(data.status),
  group: data && data.group || null,
  description: data && data.description || null,
  createdAt: data && data.createdAt || null,
  updatedAt: data && data.updatedAt || null,
});

const userStatus = (status) => {
  if (typeof status !== 'number') throw new Error('Status must be number');

  if (status === 1) {
    return 'Active';
  } if (status === 2) {
    return 'Inactive';
  }

  return null;
};

module.exports = userTransformer;
