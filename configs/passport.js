const { Strategy, ExtractJwt } = require('passport-jwt');

const User = require('../models/user');

module.exports = (passport) => {
  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET,
    passReqToCallback: true,
  };

  passport.use(new Strategy(options, async (_, payload, done) => {
    const userId = payload && payload._id || null;
    const user = await User.findOne({ _id: userId });
    if (!user) return done(null, false);
    return done(null, user);
  }));
};
