const replaceSqlInjection = (data) => {
  if (typeof data !== 'string') throw new Error('Data must be string');

  const pattern = /([^a-z0-9 ']+)/gi;
  return data.replace(pattern, '');
};

module.exports = replaceSqlInjection;
