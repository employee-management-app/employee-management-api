const { NotFound, ForBidden } = require('./errorHandler');

const User = require('../models/user');

const userRoleValidator = {};
userRoleValidator.userAdmin = async (req, res) => {
  const user = await User.findById(req.user._id).select('role').lean();
  if (!user) return NotFound(res, 'User not found');
  if (user.role !== 'admin') return ForBidden(res, "You don't habe role as admin");
};

module.exports = userRoleValidator;
