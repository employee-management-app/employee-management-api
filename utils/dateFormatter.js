const moment = require('moment');

const { isValidDate } = require('./helper');

const dateFormatter = {};
dateFormatter.startOfDay = (date) => {
  if (!isValidDate(date)) return 'Date must be valid date';
  return new Date(moment(date).startOf('day').toISOString());
};

dateFormatter.endOfDay = (date) => {
  if (!isValidDate(date)) return 'Date must be valid date';
  return new Date(moment(date).endOf('day').toISOString());
};

module.exports = dateFormatter;
