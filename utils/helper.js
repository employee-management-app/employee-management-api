/* eslint-disable no-param-reassign */
const helper = {};
helper.removeSpaceFromString = (data) => {
  if (typeof data !== 'string') throw new Error('Data must be string');
  return data.replace(/\s/g, '');
};

helper.isValidDate = (date) => date instanceof Date && !isNaN(date);

helper.shuffleArray = (datas) => {
  if (!Array.isArray(datas)) throw new Error('Data must be array');

  for (let i = datas.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [datas[i], datas[j]] = [datas[j], datas[i]];
  }

  return datas;
};

module.exports = helper;
