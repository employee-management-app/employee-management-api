const bcrypt = require('bcrypt');

const comparePasswordAsync = (password1, password2) => {
  if (typeof password1 !== 'string') throw new Error('Password 1 must be string');
  if (typeof password2 !== 'string') throw new Error('Password 2 must be string');

  return new Promise((resolve, reject) => {
    bcrypt.compare(password1, password2, (error, res) => {
      if (error) return reject(error);
      return resolve(res);
    });
  });
};

module.exports = comparePasswordAsync;
