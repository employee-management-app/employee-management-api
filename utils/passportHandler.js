const passport = require('passport');

const { Unauthorized } = require('./errorHandler');
const { payloadJwt } = require('../transformers/auth');

const passportHandler = (req, res, next) => {
  passport.authenticate('jwt', { session: false, failureRedirect: '/api/response/fail' }, (error, user) => {
    if (error) throw error;
    if (!user) return Unauthorized(res);
    req.user = payloadJwt(user);
    next();
  })(req, res, next);
};

module.exports = passportHandler;
