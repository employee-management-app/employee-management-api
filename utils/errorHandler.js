const errorHandler = {};

errorHandler.BadRequest = (res, error) => {
  res.status(400).json({ status: 'error', message: error || 'Bad Request' });
};

errorHandler.Unauthorized = (res, error) => {
  res.status(401).json({ status: 'error', message: error || 'Unauthorized' });
};

errorHandler.ForBidden = (res, error) => {
  res.status(403).json({ status: 'error', message: error || 'Forbidden' });
};

errorHandler.NotFound = (res, error) => {
  res.status(404).json({ status: 'error', message: error || 'Not Found' });
};

errorHandler.InternalServerError = (res, error) => {
  res.status(500).json({ status: 'error', message: error || 'Internal Server Error' });
};

errorHandler.UnHandler = (res, error) => {
  if (error?.name === 'ValidationError') {
    let errorMessage = null;

    if (error?.details) {
      errorMessage = error?.details[0]?.message;
    } else {
      errorMessage = error?.message;
    }

    res.status(400).json({ status: 'error', message: errorMessage });
  } else if (error?.code === 'ENOENT') {
    res.status(400).json({ status: 'error', message: error?.toString() });
  } else {
    const message = error?.toString() || 'Something technically wrong';

    if (error?.kind === 'ObjectId') {
      res.status(400).json({ status: 'error', message });
    } else {
      res.status(500).json({ status: 'error', message });
    }
  }
};

module.exports = errorHandler;
