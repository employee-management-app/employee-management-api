# EMPLOYEE MANAGEMENT API
This project was created with [Express](https://expressjs.com/)

## Table of Contents
* [How to Setup](#how-to-setup)
* [How to Run](#how-to-run)
* [Libraries](#libraries)
* [Must to Know](#must-to-know)

## How to Setup
* Setup the DB (MongoDB)
  - Download and install MongoDB, You can download MongoDB via the following [link](https://www.mongodb.com/docs/manual/installation/) or you can use docker [image](https://hub.docker.com/_/mongo) (recomendation).
  - If the download and installation process is complete, try connecting to MongoDB.
* Set all environment 
  - Create .env file
  - If the file has been created, then copy the variables below and paste them into the .env file
  ```
  # APP
  NODE_ENV=development
  PORT=5000

  # MONGO DB
  MONGO_HOST=localhost
  MONGO_PORT=27017
  MONGO_DATABASE=employee-management

  # JWT
  PRIVATE_KEY=employeemanagementapp

  # Express Session
  IS_HTTPS=true
  SECRET=employeemanagementapp
  ```
  - Please note that, the value NODE_ENV=development
* Install nvm to install npm and node with management version
* Install the package
  - ` $ npm install `

## How to Run
Run ` $ npm start ` to run the application. Make sure that the application is running on port 5000, because the frontend is connected to the backend with http://localhost:5000

## Libraries
* Encription hashing with [bcrypt](https://github.com/kelektiv/node.bcrypt.js#readme)
* Encription token with [Jsonwebtoken](https://github.com/auth0/node-jsonwebtoken#readme)
* Continues local development with [Nodemon](https://nodemon.io/)
* Database storage with [MongoDB](https://www.mongodb.com/)
* ORM DB with [mongoose](https://mongoosejs.com/)

## Must to Know
* Use permanent version package for dependencies. 
* You can test this API by using [Postman](https://www.postman.com/). You can also import collection and environment files in the postman file
